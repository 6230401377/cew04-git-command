public class bubbleSort {
    public static void main(String[] args) {
        bubbleSort bs = new bubbleSort();
        int arr[] = {94, 66, 10, 11, 29, 78, 3, 13, 79};
        bs.bubbleSortFunction(arr);
        System.out.println("Sorted array's down below");
        bs.printArray(arr);
        System.out.println("array's length is " + arr.length);
        System.out.println("the lowest value in this array is : " + arr[0]);
    }

    public void printArray(int arr[]) {
        int n = arr.length;
        for(int i = 0; i < n; i++ ) { 
            System.out.print(arr[i] + " ");
            System.out.println("");
        }
    }

    public void bubbleSortFunction(int arr[]){ 
        int n = arr.length; 
        for (int i = 0; i < n-1; i++) 
            for (int j = 0; j < n-i-1; j++) 
                if (arr[j] > arr[j+1]) 
                { 
                    // swap arr[j+1] and arr[i] 
                    int temp = arr[j]; 
                    arr[j] = arr[j+1]; 
                    arr[j+1] = temp; 
                } 
    }
}
